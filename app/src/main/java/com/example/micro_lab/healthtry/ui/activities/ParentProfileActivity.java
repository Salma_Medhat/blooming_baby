package com.example.micro_lab.healthtry.ui.activities;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.example.micro_lab.healthtry.R;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class ParentProfileActivity extends AppCompatActivity {

    TextView pname,pemail,pnum;
    ListView childlist;
    Button editProfile,addChild,addReminder;
    private RequestQueue requestQueue;
    private static final String URL = "http://192.168.1.112/HealthcareAndroid/parentProfile.php";
    private StringRequest request;
    ArrayList<String> children;
    ArrayList<Integer> childrenids;
    ArrayAdapter<String> adapter;
    AlertDialog.Builder builder;
    String pmail;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.activity_parent_profile);

        pname = (TextView) findViewById(R.id.parentname);
        pemail = (TextView) findViewById(R.id.parentemail);
        pnum = (TextView) findViewById(R.id.parentphonenum);
        childlist = (ListView) findViewById(R.id.childlist);
        editProfile = (Button) findViewById(R.id.editprofile);
        addChild = (Button) findViewById(R.id.addchild);
        addReminder = (Button) findViewById(R.id.addreminder);
        children = new ArrayList<>();
        childrenids = new ArrayList<>();

        Bundle b = getIntent().getExtras();
        pmail = b.getString("email");


        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setDisplayShowTitleEnabled(false);
            actionBar.setDisplayShowHomeEnabled(false);
        }


        requestQueue = Volley.newRequestQueue(getApplicationContext());

        request = new StringRequest(Request.Method.POST, URL, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try {
                    JSONObject jsonObject = new JSONObject(response);
                    if(jsonObject.names().get(0).equals("success")){
                        Toast.makeText(getApplicationContext(),"PARENT SUCCESS "+jsonObject.getString("success"),Toast.LENGTH_SHORT).show();
                        pemail.setText(pmail);
                        pname.setText(jsonObject.getString("success")+" "+jsonObject.getString("lname"));
                        pnum.setText(jsonObject.getString("phone"));
                        if(jsonObject.getString("error").equals("no")){
                            JSONArray babies = jsonObject.getJSONArray("babynames");
                            JSONArray babiesids = jsonObject.getJSONArray("babyids");
                            for(int i=0;i<babies.length();i++){
                                children.add(babies.getString(i));
                                childrenids.add(babiesids.getInt(i));
                            }
                            adapter = new ArrayAdapter<String>(getApplicationContext(),R.layout.childrenlist_item,R.id.childrenlinks,children);
                            childlist.setAdapter(adapter);
                            childlist.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                                @Override
                                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                                    Intent i = new Intent(getApplicationContext(),ChildProfileActivity.class);
                                    i.putExtra("childid",childrenids.get(position));
                                    i.putExtra("pmail",pmail);
                                    startActivity(i);
                                }
                            });
                        }
                    }else {
                        Toast.makeText(getApplicationContext(), "ERROR " +jsonObject.getString("error"), Toast.LENGTH_SHORT).show();
                    }

                } catch (JSONException e) {
                    Log.e("jsonExption","whaaay");
                    e.printStackTrace();
                }


            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

            }
        }){
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                HashMap<String,String> hashMap = new HashMap<String, String>();
                hashMap.put("email",pmail);

                return hashMap;
            }
        };

        requestQueue.add(request);

    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.parentmenu,menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId()==R.id.notifications){
            Toast.makeText(this, "Noy Yet Implemented", Toast.LENGTH_SHORT).show();
//            Intent i = new Intent(ParentProfileActivity.this,Notifications.class);
//            i.putExtra("email",pemail);
//            startActivity(i);
        }
        else{
            builder = new AlertDialog.Builder(ParentProfileActivity.this);
            builder.setTitle("User Logout")
                    .setMessage("Are you sure?")
                    .setCancelable(true)
                    .setNegativeButton("No", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                        }
                    })
                    .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            startActivity(new Intent(ParentProfileActivity.this,LoginActivity.class));
                        }
                    });
            AlertDialog a = builder.create();
            a.show();
        }

        return super.onOptionsItemSelected(item);
    }
}
