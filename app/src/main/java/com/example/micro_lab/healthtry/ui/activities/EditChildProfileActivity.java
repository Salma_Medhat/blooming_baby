package com.example.micro_lab.healthtry.ui.activities;

import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.PopupMenu;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.example.micro_lab.healthtry.R;

import org.json.JSONException;
import org.json.JSONObject;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;

public class EditChildProfileActivity extends AppCompatActivity {

    EditText bday,btype,gender,lname,fname,bplace;
    Calendar myCalendar = Calendar.getInstance();
    DatePickerDialog.OnDateSetListener date;
    int childid;
    String pemail;
    AlertDialog.Builder builder;
    private RequestQueue requestQueue;
    private static final String URL = "http://192.168.1.112/HealthcareAndroid/childProfile.php";
    private static final String URL2 = "http://192.168.1.112/HealthcareAndroid/editChildProfile.php";
    private StringRequest request;
    Button submit;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_child_profile);
        bday = (EditText) findViewById(R.id.editbday);
        btype = (EditText) findViewById(R.id.editbtype);
        gender = (EditText) findViewById(R.id.editgender);
        fname = (EditText) findViewById(R.id.editfname);
        lname = (EditText) findViewById(R.id.editlname);
        bplace = (EditText) findViewById(R.id.editbplace);
        submit = (Button) findViewById(R.id.editSubmit);

        Bundle b = getIntent().getExtras();
        childid = b.getInt("childid");
        pemail = b.getString("pmail");

        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setDisplayShowTitleEnabled(false);
            actionBar.setDisplayShowHomeEnabled(false);
        }

        requestQueue = Volley.newRequestQueue(getApplicationContext());

        request = new StringRequest(Request.Method.POST, URL, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try {
                    JSONObject jsonObject = new JSONObject(response);
                    if(jsonObject.names().get(0).equals("success")){
//                        fname = jsonObject.getString("success");
//                        lname = jsonObject.getString("lname");
                        fname.setText(jsonObject.getString("success"));
                        lname.setText(jsonObject.getString("lname"));
                        bplace.setText(jsonObject.getString("bplace"));
                        btype.setText(jsonObject.getString("btype"));
                        if(jsonObject.getString("gender").equals("g")){
                            gender.setText("Girl");
                        }else{
                            gender.setText("Boy");
                        }
                        bday.setText(jsonObject.getString("bday"));

                    }else {
                        Toast.makeText(getApplicationContext(), "ERROR " +jsonObject.getString("error"), Toast.LENGTH_SHORT).show();
                    }

                } catch (JSONException e) {
                    Log.e("jsonExption","whaaay");
                    e.printStackTrace();
                }


            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

            }
        }){
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                HashMap<String,String> hashMap = new HashMap<String, String>();
                hashMap.put("childid",Integer.toString(childid));

                return hashMap;
            }
        };

        requestQueue.add(request);

        gender.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                PopupMenu pm = new PopupMenu(EditChildProfileActivity.this,btype);
                getMenuInflater().inflate(R.menu.genderpopup,pm.getMenu());
                pm.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                    @Override
                    public boolean onMenuItemClick(MenuItem item) {
                        if(item.getItemId()==R.id.boy){
                            gender.setText("Boy");
                        }else{
                            gender.setText("Girl");
                        }
                        return false;
                    }
                });
                pm.show();
            }
        });

        btype.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                PopupMenu pm = new PopupMenu(EditChildProfileActivity.this,btype);
                getMenuInflater().inflate(R.menu.btypepopup,pm.getMenu());
                pm.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                    @Override
                    public boolean onMenuItemClick(MenuItem item) {
                        if (item.getItemId()==R.id.Aplus){
                            btype.setText("A+");
                        }else if(item.getItemId()==R.id.Bplus){
                            btype.setText("B+");
                        } else if(item.getItemId()==R.id.Oplus){
                            btype.setText("O+");
                        }else if(item.getItemId()==R.id.ABplus){
                            btype.setText("AB+");
                        }else if(item.getItemId()==R.id.Aminus){
                            btype.setText("A-");
                        }else if(item.getItemId()==R.id.Bminus){
                            btype.setText("B-");
                        }else if(item.getItemId()==R.id.Ominus){
                            btype.setText("O-");
                        }else{
                            btype.setText("AB-");
                        }
                        return false;
                    }
                });
                pm.show();
            }
        });

        date = new DatePickerDialog.OnDateSetListener() {

            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear,
                                  int dayOfMonth) {
                // TODO Auto-generated method stub
                myCalendar.set(Calendar.YEAR, year);
                myCalendar.set(Calendar.MONTH, monthOfYear);
                myCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
                updateLabel();
            }

        };

        bday.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new DatePickerDialog(EditChildProfileActivity.this, date, myCalendar
                        .get(Calendar.YEAR), myCalendar.get(Calendar.MONTH),
                        myCalendar.get(Calendar.DAY_OF_MONTH)).show();
            }
        });

        requestQueue = Volley.newRequestQueue(getApplicationContext());

        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                request = new StringRequest(Request.Method.POST, URL2, new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            JSONObject jsonObject = new JSONObject(response);
                            if(jsonObject.names().get(0).equals("success")){
                                Toast.makeText(EditChildProfileActivity.this,jsonObject.getString("success") , Toast.LENGTH_SHORT).show();
                                Intent i = new Intent(EditChildProfileActivity.this,ChildProfileActivity.class);
                                i.putExtra("childid",childid);
                                i.putExtra("pmail",pemail);
                                startActivity(i);

                            }else {
                                Toast.makeText(getApplicationContext(), "ERROR " +jsonObject.getString("error"), Toast.LENGTH_SHORT).show();
                            }

                        } catch (JSONException e) {
                            Log.e("jsonExption","whaaay");
                            e.printStackTrace();
                        }


                    }
                }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {

                    }
                }){
                    @Override
                    protected Map<String, String> getParams() throws AuthFailureError {
                        HashMap<String,String> hashMap = new HashMap<String, String>();
                        hashMap.put("childid",Integer.toString(childid));
                        hashMap.put("first_name",fname.getText().toString());
                        hashMap.put("last_name",lname.getText().toString());
                        hashMap.put("birth_place",bplace.getText().toString());
                        hashMap.put("birthdate",bday.getText().toString());
                        hashMap.put("gender",gender.getText().toString());
                        hashMap.put("blood_type",btype.getText().toString());
                        return hashMap;
                    }
                };

                requestQueue.add(request);
            }
        });

    }
    private void updateLabel() {

        String myFormat = "yyyy-MM-dd"; //In which you need put here
        SimpleDateFormat sdf = new SimpleDateFormat(myFormat);

        bday.setText(sdf.format(myCalendar.getTime()));
    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu,menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId()==R.id.parentprofile){
            Intent i = new Intent(EditChildProfileActivity.this,ParentProfileActivity.class);
            i.putExtra("email",pemail);
            startActivity(i);
        }
        else{
            builder = new AlertDialog.Builder(EditChildProfileActivity.this);
            builder.setTitle("User Logout")
                    .setMessage("Are you sure?")
                    .setCancelable(true)
                    .setNegativeButton("No", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                        }
                    })
                    .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            startActivity(new Intent(EditChildProfileActivity.this,LoginActivity.class));
                        }
                    });
            AlertDialog a = builder.create();
            a.show();
        }

        return super.onOptionsItemSelected(item);
    }

}
