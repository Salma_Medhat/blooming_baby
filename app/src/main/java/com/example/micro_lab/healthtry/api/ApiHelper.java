package com.example.micro_lab.healthtry.api;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;

import org.json.JSONObject;

/**
 * Created by Salma Medhat on 4/13/2017.
 */

public class ApiHelper {

    public static void login(String userName, String password, final ResponseListener responseListener){
        AndroidNetworking.get(ApiEndPoints.Login)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        //asd//a/
                        ///
                        responseListener.onSuccess(null);

                    }

                    @Override
                    public void onError(ANError anError) {

                    }
                });
    }

    interface ResponseListener{
        void onSuccess(Object object);
        void onFailed(String errorMsg);
    }
}
