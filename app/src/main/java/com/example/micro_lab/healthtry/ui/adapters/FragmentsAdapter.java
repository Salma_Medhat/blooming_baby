package com.example.micro_lab.healthtry.ui.adapters;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.util.Log;

import com.example.micro_lab.healthtry.ui.fragments.SigninFragment;
import com.example.micro_lab.healthtry.ui.fragments.SignupFragment;

/**
 * Created by micro-lab on 4/7/2017.
 */

public class FragmentsAdapter extends FragmentPagerAdapter {
    public FragmentsAdapter(FragmentManager fm) {
        super(fm);
    }
    // set data on every fragment
    @Override
    public Fragment getItem(int index) {
        Log.d("index", "" + index);
        // TODO Auto-generated method stub
        if(index == 0)
            return new SigninFragment();
        if(index == 1)
            return new SignupFragment();

        return null;
    }
    // fragments(tabs) num
    @Override
    public int getCount() {
        // TODO Auto-generated method stub
        return 2;
    }
}
