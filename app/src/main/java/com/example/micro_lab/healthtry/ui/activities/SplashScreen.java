package com.example.micro_lab.healthtry.ui.activities;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.example.micro_lab.healthtry.R;
import com.example.micro_lab.healthtry.core.SessionManager;

public class SplashScreen extends AppCompatActivity {

    Thread t;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash_screen);
        t = new Thread(){
            @Override
            public void run() {
                try {
                    sleep(3000);

                    if(SessionManager.getInstance().isLogin()){
                        Intent i = new Intent(SplashScreen.this,ParentProfileActivity.class);
                        startActivity(i);
                    }else{
                        Intent i = new Intent(SplashScreen.this,LoginActivity.class);
                        startActivity(i);
                    }

                    finish();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        };
        t.start();
    }
}
