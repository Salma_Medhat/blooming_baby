package com.example.micro_lab.healthtry.ui.fragments;

import android.content.Context;
import android.content.Intent;
import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.support.v7.view.ContextThemeWrapper;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.example.micro_lab.healthtry.R;
import com.example.micro_lab.healthtry.ui.activities.ParentProfileActivity;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class SignupFragment extends Fragment {

    EditText fn,ln,email,pass1,pass2,phonenum;
    Button submit;
    private RequestQueue requestQueue;
    private static final String URL = "http://192.168.1.112/HealthcareAndroid/signup.php";
    private StringRequest request;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        final View view;
        final Context contextThemeWrapper = new ContextThemeWrapper(getActivity(), R.style.FragTheme);
        LayoutInflater localInflater = inflater.cloneInContext(contextThemeWrapper);
        view = localInflater.inflate(R.layout.activity_signup, container, false);

        fn = (EditText) view.findViewById(R.id.fname);
        ln = (EditText) view.findViewById(R.id.lname);
        pass1 = (EditText) view.findViewById(R.id.regpass);
        pass2 = (EditText) view.findViewById(R.id.regpass2);
        email = (EditText) view.findViewById(R.id.regemail);
        phonenum = (EditText) view.findViewById(R.id.phonenum);
        submit = (Button) view.findViewById(R.id.regsubmit);

        requestQueue = Volley.newRequestQueue(getActivity().getApplicationContext());

        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if(pass1.getText().toString().equals(pass2.getText().toString())){
                    request = new StringRequest(Request.Method.POST, URL, new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {
                            try {
                                JSONObject jsonObject = new JSONObject(response);
                                if(jsonObject.names().get(0).equals("success")){
                                    Toast.makeText(getActivity().getApplicationContext(),"SUCCESS "+jsonObject.getString("success"),Toast.LENGTH_SHORT).show();
                                    Intent i = new Intent(getActivity().getApplicationContext(),ParentProfileActivity.class);
                                    i.putExtra("email",email.getText().toString());
                                    startActivity(i);
                                }else {
                                    Toast.makeText(getActivity().getApplicationContext(), "ERROR " +jsonObject.getString("error"), Toast.LENGTH_SHORT).show();
                                }

                            } catch (JSONException e) {
                                Log.e("jsonExption","whaaay");
                                e.printStackTrace();
                            }


                        }
                    }, new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {

                        }
                    }){
                        @Override
                        protected Map<String, String> getParams() throws AuthFailureError {
                            HashMap<String,String> hashMap = new HashMap<String, String>();
                            hashMap.put("email",email.getText().toString());
                            hashMap.put("pass",pass1.getText().toString());
                            hashMap.put("fname",fn.getText().toString());
                            hashMap.put("lname",ln.getText().toString());
                            hashMap.put("phonenum",phonenum.getText().toString());

                            return hashMap;
                        }
                    };

                    requestQueue.add(request);
                }
                else{
                    Toast.makeText(contextThemeWrapper, "Passwords Do Not Match", Toast.LENGTH_SHORT).show();
                }


            }
        });

        return view;
    }
}
