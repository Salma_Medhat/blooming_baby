package com.example.micro_lab.healthtry.ui.fragments;

import android.content.Context;
import android.content.Intent;
import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.support.v7.view.ContextThemeWrapper;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.example.micro_lab.healthtry.R;
import com.example.micro_lab.healthtry.ui.activities.ParentProfileActivity;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class SigninFragment extends Fragment {
    private EditText email,password;
    private Button submit;
    //eli hab3at 3aleeh el requests betoo3i: POST, GET keda ya3ni
    private RequestQueue requestQueue;
    //el PHP file eli hayraga3li JSON code
    private static final String URL = "http://192.168.1.112/HealthcareAndroid/signin.php";
    //ha create beeh el request eli ha7otaha fel queue
    private StringRequest request;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        final View rootView;

        final Context contextThemeWrapper = new ContextThemeWrapper(getActivity(), R.style.FragTheme);
        //Theme mo3ayan 3ashan el fragments mesh beyemshy ma3aha el theme el default
        LayoutInflater localInflater = inflater.cloneInContext(contextThemeWrapper);
        rootView = localInflater.inflate(R.layout.fragment_login, container, false);



        email = (EditText) rootView.findViewById(R.id.email);
        password = (EditText) rootView.findViewById(R.id.pass);
        submit = (Button) rootView.findViewById(R.id.submit);

        //Volley dah 7aga ba7otaha fel Gradle keda betsa3edni fel inserting w el retrieving men el database. El mafrood enaha saree3a.
        //Check el Gradle Build fel akher ana 3amlalo compile ma3 el android 3ady.
        requestQueue = Volley.newRequestQueue(getActivity().getApplicationContext());

        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                //request a-run beeha el PHP code 3ashan yegeeli el JSON. El inputs eli el file mestaneeha ba7otaha fel getParams() ta7t.
                request = new StringRequest(Request.Method.POST, URL, new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            //El connection eshta3'al w gali JSON code kowayes mafiish error.
                            JSONObject jsonObject = new JSONObject(response);
                            //awel input fel JSON ana bakhali el key beta3ha 'success' 3ashan a-check en feeh results mesh error.
                            if(jsonObject.names().get(0).equals("success")){
                                //el JSON eli ana ba3taha ma7toot f 7aga shabah el hashmap fa howa key and value. momken el value teb2a string aw array
                                //bas el array keda hayeb2a JSONArray zai ma akhadna fel course.-->Check el ParentProfileActivity feeh array.
                                Toast.makeText(getActivity().getApplicationContext(),"SUCCESS "+jsonObject.getString("success"),Toast.LENGTH_SHORT).show();
                                Intent i = new Intent(getActivity().getApplicationContext(),ParentProfileActivity.class);
                                i.putExtra("email",email.getText().toString());
                                startActivity(i);

                            }else {
                                //ay error bakhali el key beta3o 'error' w howa haygeeb el error eh sababo (ana already katba fel php)
                                Toast.makeText(getActivity().getApplicationContext(), "ERROR " +jsonObject.getString("error"), Toast.LENGTH_SHORT).show();
                            }

                        } catch (JSONException e) {
                            Log.e("jsonExption","whaaay");
                            e.printStackTrace();
                        }


                    }
                }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {

                    }
                }){
                    @Override
                    protected Map<String, String> getParams() throws AuthFailureError {
                        HashMap<String,String> hashMap = new HashMap<String, String>();
                        hashMap.put("email",email.getText().toString());
                        hashMap.put("password",password.getText().toString());

                        return hashMap;
                    }
                };

                requestQueue.add(request);
            }
        });
        return rootView;
    }
}
