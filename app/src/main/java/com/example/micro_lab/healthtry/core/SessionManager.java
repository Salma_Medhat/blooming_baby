package com.example.micro_lab.healthtry.core;

import android.content.Context;
import android.content.SharedPreferences;

/**
 * Created by Salma Medhat on 4/13/2017.
 */

public class SessionManager {

    private static SessionManager instance;

    private static final String PREF_NAME = "preference";

    private static final String KEY_IS_LOGIN = "is_login";
    private static final String KEY_USER_NAME = "user_name";

    private SharedPreferences preferences;
    private SharedPreferences.Editor editor;

    private SessionManager (){
        preferences = BloomingBabyApp.getInstance().getSharedPreferences(PREF_NAME, Context.MODE_PRIVATE);
        editor = preferences.edit();
    }

    public static SessionManager getInstance(){
        if(instance == null){
            instance = new SessionManager();
        }
        return instance;
    }

    public void login(String name){
        editor.putBoolean(KEY_IS_LOGIN, true);
        editor.putString(KEY_USER_NAME, name);
        editor.commit();
    }

    public boolean isLogin(){
        return preferences.getBoolean(KEY_IS_LOGIN, false);
    }
}
