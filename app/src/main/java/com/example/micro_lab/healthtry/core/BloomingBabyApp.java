package com.example.micro_lab.healthtry.core;

import android.app.Application;

import com.androidnetworking.AndroidNetworking;

/**
 * Created by Salma Medhat on 4/13/2017.
 */

public class BloomingBabyApp  extends Application {

    private static BloomingBabyApp instance;

    public static BloomingBabyApp getInstance(){
        return instance;
    }

    @Override
    public void onCreate() {
        super.onCreate();

        instance = this;

        AndroidNetworking.initialize(getApplicationContext());
    }
}
