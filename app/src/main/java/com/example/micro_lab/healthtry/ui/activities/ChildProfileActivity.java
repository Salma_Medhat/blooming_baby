package com.example.micro_lab.healthtry.ui.activities;


import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.example.micro_lab.healthtry.R;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;


public class ChildProfileActivity extends AppCompatActivity {

    TextView chname,bplace,btype,gender,bday;
    Button editProfile,lastReport,deleteChild;
    private RequestQueue requestQueue;
    private static final String URL = "http://192.168.1.112/HealthcareAndroid/childProfile.php";
    private static final String DeleteURL = "http://192.168.1.112/HealthcareAndroid/deleteChild.php";
    private StringRequest request;
    AlertDialog.Builder builder;
    int childid;
    String pemail;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.activity_child_profile);
        chname = (TextView) findViewById(R.id.childname);
        bplace = (TextView) findViewById(R.id.childbirthplace);
        btype = (TextView) findViewById(R.id.childbloodtype);
        gender = (TextView) findViewById(R.id.childgender);
        bday = (TextView) findViewById(R.id.birthday);
        editProfile = (Button) findViewById(R.id.editchildprofile);
        lastReport = (Button) findViewById(R.id.latestreport);
        deleteChild = (Button) findViewById(R.id.deletechild);

        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setDisplayShowTitleEnabled(false);
            actionBar.setDisplayShowHomeEnabled(false);
        }

        Bundle b = getIntent().getExtras();
        childid = b.getInt("childid");
        pemail = b.getString("pmail");

        deleteChild.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                requestQueue = Volley.newRequestQueue(getApplicationContext());

                request = new StringRequest(Request.Method.POST, DeleteURL, new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            JSONObject jsonObject = new JSONObject(response);
                            if(jsonObject.names().get(0).equals("success")){
                                Toast.makeText(ChildProfileActivity.this,jsonObject.getString("success") , Toast.LENGTH_SHORT).show();
                                Intent i = new Intent(ChildProfileActivity.this,ParentProfileActivity.class);
                                i.putExtra("email",pemail);
                                startActivity(i);
                            }else {
                                Toast.makeText(getApplicationContext(), "ERROR " +jsonObject.getString("error"), Toast.LENGTH_SHORT).show();
                            }

                        } catch (JSONException e) {
                            Log.e("jsonExption","whaaay");
                            e.printStackTrace();
                        }


                    }
                }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                          error.printStackTrace();
                    }
                }){
                    @Override
                    protected Map<String, String> getParams() throws AuthFailureError {
                        HashMap<String,String> hashMap = new HashMap<String, String>();
                        hashMap.put("childid",Integer.toString(childid));

                        return hashMap;
                    }
                };

                requestQueue.add(request);
            }
        });

        editProfile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(ChildProfileActivity.this,EditChildProfileActivity.class);
                i.putExtra("childid",childid);
                i.putExtra("pmail",pemail);
                startActivity(i);
            }
        });

        requestQueue = Volley.newRequestQueue(getApplicationContext());

        request = new StringRequest(Request.Method.POST, URL, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try {
                    JSONObject jsonObject = new JSONObject(response);
                    if(jsonObject.names().get(0).equals("success")){
                     chname.setText(jsonObject.getString("success")+" "+jsonObject.getString("lname"));
                     bplace.setText(jsonObject.getString("bplace"));
                     btype.setText(jsonObject.getString("btype"));
                        if(jsonObject.getString("gender").equals("g")){
                            gender.setText("Girl");
                        }else{
                            gender.setText("Boy");
                        }
                     bday.setText(jsonObject.getString("bday"));

                    }else {
                        Toast.makeText(getApplicationContext(), "ERROR " +jsonObject.getString("error"), Toast.LENGTH_SHORT).show();
                    }

                } catch (JSONException e) {
                    Log.e("jsonExption","whaaay");
                    e.printStackTrace();
                }


            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

            }
        }){
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                HashMap<String,String> hashMap = new HashMap<String, String>();
                hashMap.put("childid",Integer.toString(childid));

                return hashMap;
            }
        };

        requestQueue.add(request);

    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu,menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId()==R.id.parentprofile){
            Intent i = new Intent(ChildProfileActivity.this,ParentProfileActivity.class);
            i.putExtra("email",pemail);
            startActivity(i);
        }
        else{
            builder = new AlertDialog.Builder(ChildProfileActivity.this);
            builder.setTitle("User Logout")
                    .setMessage("Are you sure?")
                    .setCancelable(true)
                    .setNegativeButton("No", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                        }
                    })
                    .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            startActivity(new Intent(ChildProfileActivity.this,LoginActivity.class));
                        }
                    });
            AlertDialog a = builder.create();
            a.show();
        }

        return super.onOptionsItemSelected(item);
    }

}
